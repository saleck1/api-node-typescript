import express, {Application} from "express";
import cors from "cors"
import bodyParser from "body-parser";
import routers from "./routers";
class App{


    public app: Application = express()

    constructor() {
        console.log('Hello typescript !')
        this.Configuration()
        this.ConfigRouters()
        this.initServer()
    }

    private Configuration = ():void =>{
        this.app.use(cors())
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({extended: false}))
    }

    private ConfigRouters = ():void =>{
        this.app.use('/api/v2/',routers)
    }

    private initServer=(): void =>{
        const port: number = 8060 || process.env.PORT
        this.app.listen(port, () => console.log("server listening on port %d", port))
    }


}
export default new App()
