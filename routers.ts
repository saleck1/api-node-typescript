import express from "express";
import {Request, Response} from "express";
import axios from "axios"
import Controlers from "./Controlers";


class Routers{
    constructor() {
        this.path()
    }

    public route = express.Router({caseSensitive: true})

    private  path =(): void =>{
        this.getAllParkings()
        this.getParkingById()
        this.postParkings()
        this.putParkingById()
        this.deleteParkingById()
    }

    private getAllParkings = () :void =>{
        this.route.get('/parkings',async (req: Request, res: Response)=>{
            try {
                const data = new Controlers().getAllParkings()

                res.status(200).json(await data)
            }catch (e: any){
                const ERROR = JSON.parse(e.message);
                res.status(ERROR.status).json(ERROR.message)
            }


        });
    };

    private getParkingById = () :void =>{
        this.route.get('/parkings/:id',async (req,res) => {
            const id = Number(req.params.id);
        try {

            const data = new Controlers().getParkingById(id);

            res.status(200).json(await data);
        }catch (e:any){
            const err = JSON.parse(e.message)
            res.status(err.status).json(err)
        }

        })
    }

    private postParkings = () :void =>{
        this.route.post('/parkings',async (req,res)=>{
        try {
            const data = await new Controlers().postParkings()
            data.push(req.body)
            res.status(200).json(data)
        }catch (e: any){
            const err= JSON.parse(e.message)
            res.status(err.status).json(err.message)
        }

        })
    }

    private putParkingById = (): void =>{
        this.route.put('/parkings/:id',new Controlers().putParkingById)
    }

    private deleteParkingById = () :void =>{
        this.route.delete('/parkings/:id',new Controlers().deleteParkingById)
    }


}
export default new Routers().route
