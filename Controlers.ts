import axios, {AxiosError} from "axios";
import {Request, Response} from "express";

export default class Controlers{

    constructor() {}

    private getRessource(): Promise<any> {
        return axios.get('https://raw.githubusercontent.com/RayedB/node-express-api/master/parkings.json')
    }

    public getAllParkings(): any{
        return this.getRessource()
            .then(ress => ress.data)
            .catch((err: AxiosError) => {
                throw new Error(JSON.stringify(err));
            })
            .finally(()=>console.log('fin'))
    }

    public getParkingById(id: number): any{
        return this.getRessource()
            .then(result=> result.data)
            .then( response => response.find( (x: any) => x.id === id))
            .catch(err=> {
                throw new Error((JSON.stringify(err)))
            }).finally( () =>
                console.log("it's done")
            );

    }

    public postParkings():any{
        return this.getRessource()
            .then(res=> res.data)
            .catch(err=> {
                throw new Error((JSON.stringify(err)))
            })
            .finally(()=>console.log("It's done"))
    }


    public putParkingById = async (req: Request,res: Response)=>{
        try {
            const data = axios.get('https://raw.githubusercontent.com/RayedB/node-express-api/master/parkings.json')
                .then(ress=>ress.data)
                .catch((err)=>{
                    throw new Error(JSON.stringify(err))
                })
                .finally(()=>console.log('fin'))

            const data1 = await data
            const id = Number(req.params.id)
            let parking = data1.find((p:any) => p.id === id)

            /*const { name, city, type} = req.body;

            parking = {
                id: req.params.id,
                name: name,
                city: city,
                type: type
            }*/
            parking.name =req.body.name
            parking.city =req.body.city
            parking.type =req.body.type

            res.status(200).json(parking)
        }catch (e: any) {
            const Error = JSON.parse(e.message)
            res.status(Error.status).json(Error.message)
        }

    }

    public deleteParkingById = async (req:Request,res:Response): Promise<any> =>{
        try {
            const data = axios.get('https://raw.githubusercontent.com/RayedB/node-express-api/master/parkings.json')
                .then(ress=>ress.data)
                .catch((err)=> {
                    throw new Error(JSON.stringify(err))
                } )
                .finally(()=>console.log('fin'))

            const id = Number(req.params.id)
            const data1 = await Promise.all(await data)
            let parking = data1.find(parking => parking.id === id)
            data1.splice(data1.indexOf(parking),1)

            res.status(200).json(data1)
        }catch (e: any) {
            const Error = JSON.parse(e.message)
            res.status(Error.status).json(Error.message)
        }

    }
}
